from setuptools import find_packages, setup

entry_points = {
    'console_scripts': [
        'scrape_parks = campcrawler.cli:scrape_parks',
        'scrape_reservations = campcrawler.cli:scrape_reservations',
        'scrape_sites = campcrawler.cli:scrape_sites',
    ],
}

install_requires = [
    'scrapy',
    'txpostgres',
    'python-dateutil',
    'asyncpg',
    'googlemaps',
    'pillow',
    'psycopg2-binary',
]

dev_requires = [
    'pyramid_debugtoolbar'
]

setup(
    name='campin',
    version='1.0.0',
    description='',
    packages=find_packages(),
    include_package_data=True,
    install_requires=install_requires,
    extras_require={
        'dev': dev_requires,
    },
    entry_points=entry_points,
)
