"""Pipeline for storing park information."""
import logging

import googlemaps
from psycopg2._json import Json
from psycopg2.extensions import register_adapter
from txpostgres import txpostgres
from twisted.internet.defer import ensureDeferred

register_adapter(dict, Json)
log = logging.getLogger(__name__)


class ParkPipeline(object):
    """
    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )
    """

    def open_spider(self, spider):
        # Assigning instance attribute here because this method is called
        # when the spider opens and will always called before process_item.
        self._gmaps = googlemaps.Client(spider.gmaps_apikey)
        self._conn = txpostgres.Connection()
        self._db = self._conn.connect(**spider.db_settings)

    def process_item(self, item, spider):
        log.debug('Processing item: {}'.format(item['park_name']))
        d = ensureDeferred(self._process_park(item))
        return d

    async def _process_park(self, item):
        exists = await self._exists(item)
        if not exists:
            item = self._set_item_distance(item)
            park_id = await self._insert_park(item)
            await self._insert_travel_times(park_id, item)
        else:
            await self._update_park(item)
        return item

    async def _exists(self, item):
        log.debug('Checking if park exists. {}'.format(item['park_name']))
        results = await self._conn.runQuery(
            """
            SELECT count(*)
            FROM parks
            WHERE park_name = %(park_name)s
        """, {'park_name': item['park_name']}
        )
        return results[0][0] > 0

    async def _insert_park(self, item):
        query = """
            INSERT INTO parks(
                park_name,
                activities,
                facilities,
                usages,
                operating_date_from,
                operating_date_to,
                parent_park_id
            )VALUES(
                %(park_name)s,
                %(activities)s,
                %(facilities)s,
                %(usages)s,
                %(operating_date_from)s,
                %(operating_date_to)s,
                %(parent_park_id)s
            ) RETURNING park_id
        """

        if item.get('parent_park_name'):
            parent_park_id = await self._park_id(item['parent_park_name'])
            item['parent_park_id'] = parent_park_id

        log.debug('Inserting new park. {}'.format(item['park_name']))
        results = await self._conn.runQuery(query, dict(item))
        return results[0][0]

    async def _update_park(self, item):
        query = """
            UPDATE parks
            SET activities = %(activities)s,
                facilities = %(facilities)s,
                usages = %(usages)s,
                operating_date_from = %(operating_date_from)s,
                operating_date_to = %(operating_date_to)s
              WHERE park_name = %(park_name)s
        """
        log.debug('Updating existing park. {}'.format(item['park_name']))
        await self._conn.runOperation(query, dict(item))

    async def _park_id(self, park_name):
        results = await self._conn.runQuery(
            'SELECT park_id FROM parks WHERE park_name = %(park_name)s',
            {'park_name': park_name}
        )

        if results:
            return results[0][0]
        else:
            return None

    def _set_item_distance(self, item):
        log.debug(
            'Finding distance to Toronto. {}'.format(item['park_name'])
        )
        item['travel_times'] = {
            'Toronto': self._park_distance(item['park_name'])
        }
        return item

    async def _insert_travel_times(self, park_id, item):
        if item['travel_times']:
            for origin, duration in item['travel_times'].items():
                await self._conn.runOperation("""
                INSERT INTO park_drive_hours(park_id, origin, drive_hours)
                VALUES(%(park_id)s, %(origin)s, %(duration)s)
                """, {
                    'park_id': park_id,
                    'origin': origin,
                    'duration': duration
                })

    def _park_distance(self, park_name):
        distance = self._gmaps.distance_matrix(
            units='metric',
            origins='Toronto, Ontario',
            destinations='{} Provincial Park, Ontario, Canada'.
            format(park_name)
        )

        try:
            distance = (distance['rows'][0]['elements'][0]['duration']['text'])
        except (IndexError, KeyError):
            log.warning(
                'Could not find distance from Toronto. Park: {}'.
                format(park_name)
            )
            distance = None

        return distance
